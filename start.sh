#!/bin/sh

# Start apache in the background
apachectl start

# Start cron in the background
crond

# Start a process to watch for changes in the library with inotify
(
while true; do
    inotifywatch /media
    php /var/www/localhost/htdocs/ampache/bin/catalog_update.inc -a
    sleep 300
done
) 
